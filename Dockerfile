FROM nginx

RUN mkdir /usr/share/nginx/html/allFiles

#COPY ./dist/ /usr/share/nginx/html/frontoffice
COPY ./dist/ /usr/share/nginx/html
#COPY ./dist/ /usr/share/nginx/html/frontoffice

#COPY ./dist/ /var/www/html
#COPY /../close-apps-back-office/dist/ /usr/share/nginx/html/backoffice
#COPY ././allFiles/ /usr/share/nginx/html/allFiles

# Copy our custom nginx config
# COPY /nginx-default-custom.conf /etc/nginx/conf.d/default.conf


# Expose port 80 to the Docker host, so we can access it
# from the outside.
EXPOSE 82
ENTRYPOINT ["nginx","-g","daemon off;"]
