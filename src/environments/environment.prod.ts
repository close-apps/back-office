/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import { HttpHeaders } from "@angular/common/http";

export const environment = {
  production: true,

  httpOptions: {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: "my-auth-token",
    }),
  },
  // BASE_URL: "https://close-apps-back-end.herokuapp.com/",
  //BASE_URL: "http://161.97.155.52:9000/",
  //BASE_URL: "http://app-backend:8080/",
  // BASE_URL: "http://app-backend:9000/",
  BASE_URL: "http://close-apps.com:8080/",

  //BASE_URL: "https://spasuce.com/api/projet-spasuce-stg-0.1/",
  //BASE_URL:"https://spasuce.com/api/projet-spasuce-0.1/",
  //BASE_URL:"https://207.180.196.73:8080/api/projet-spasuce-0.1/",
  //BASE_URL: "http://localhost:8080/",

  //BASE_URL:"https://spasuce-api.herokuapp.com/",

  user: "user",
  enseigneId: "enseigneId",
  userEnseigne: "userEnseigne",
  fichier: "fichier",
  CLASSE_A: "CLASSE_A",
  CLASSE_B: "CLASSE_B",
  CLASSE_C: "CLASSE_C",
  enseigne: "enseigne",
  historiqueSouscriptionEnseigne: "historiqueSouscriptionEnseigne",
  appelDoffres: "appelDoffres",
  appelDoffresRestreint: "appelDoffresRestreint",
  offre: "offre",
  role: "role",
  typeAction: "typeAction",

  secteurActivite: "secteurActivite",
  uniteTemps: "uniteTemps",
  typeRegleSecuriteTampon: "typeRegleSecuriteTampon",
  typeRegleSecuriteCarte: "typeRegleSecuriteCarte",
  partenaires: "partenaires",
  souscription: "souscription",
  pallierForfait: "pallierForfait",
  dureeForfait: "dureeForfait",
  faq: "faq",
  responses: "responses",
  questions: "questions",
  secteurDactiviteAppelDoffres: "secteurDactiviteAppelDoffres",
  secteurDactiviteEntreprise: "secteurDactiviteEntreprise",
  valeurCritereOffre: "valeurCritereOffre",
  contenuStatique: "contenuStatique",
  objetNousContacter: "objetNousContacter",
  nousContacter: "nousContacter",
  datasOffresNotFound: "Aucune offre trouvée. Merci d'en creer !!!",

  // les typeAppelDoffresCode
  RESTREINT: "RESTREINT",
  GENERAL: "GENERAL",

  // les etats code

  BROUILLON: "BROUILLON",
  ENTRANT: "ENTRANT",
  TERMINER: "TERMINER",

  FOURNISSEUR: "FOURNISSEUR",
  ADMINISTRATEUR: "ADMINISTRATEUR",
  CLIENTE: "CLIENTE",
  MIXTE: "MIXTE",

  NOUS_CONTACTER: "NOUS_CONTACTER",
  NOS_CONDITIONS: "NOS_CONDITIONS",
  COMMENT_CA_MARCHE: "COMMENT_CA_MARCHE",
  QUI_SOMMES_NOUS: "QUI_SOMMES_NOUS",
  SERVICES_FONCTIONNEMENT: "SERVICES_FONCTIONNEMENT",
  FONCTIONNEMENT: "FONCTIONNEMENT",
  PRESENTATION: "PRESENTATION",
  REALISATIONS: "REALISATIONS",

  // token
  token: "token",
  email: "email",

  // navigation
  ordre: "element",

  ACCUEIL: "ACCUEIL",
  SOUMETTRE: "SOUMETTRE",
  CREER: "CREER",
  ENREGISTRER: "ENREGISTRER",
  NOUVEAU: "NOUVEAU",
  VISUALISER: "VISUALISER",
  EDITER: "EDITER",
  MODIFIER: "MODIFIER",
  RETIRER: "RETIRER",
  ANNULER: "ANNULER",
  //TEMPS_TOAST : 3000, // 3s
  TEMPS_TOAST: 5000, // 3s
  TEMPS_NOTIFICATIONS: 30000, // 30s

  // message de retour
  AUCUNE_DUREE: "AUCUNE DUREE SAISIE",

  // notifications
  primary: "primary",
  success: "success",
  warning: "warning",
  danger: "danger",
  info: "info",
  //**********************

  // critereFiltre
  TOUS: "TOUS",
  SOUMISSIONNER: "SOUMISSIONNER",
  SELECTIONNER: "SELECTIONNER",
  NON_SOUMISSIONNER: "NON SOUMISSIONNER",
  LISTES_CRITERES_FILTRES: [
    "TOUS",
    "SOUMISSIONNER",
    "NON SOUMISSIONNER",
    "SELECTIONNER",
  ],

  isAuth: "isAuth",
  authenticatedUser: "authenticatedUser",
  login: "login",
  nom: "nom",
  NBRE_MAXI_USER_PAR_ENTREPRISE: 5,

  // CRUD
  create: "/create",
  update: "/update",
  getBase64Files: "/getBase64Files",
  getByCriteria: "/getByCriteria",
  getBasicStat: "/getBasicStat",
  getStatistiques: "/getStatistiques",
  getStats: "/getStats",
  delete: "/delete",
  forceDelete: "/forceDelete",
  checkEmailWithTokenIsValid: "/checkEmailWithTokenIsValid",
  updateSecteurDactivite: "/updateSecteurDactivite",

  //AUTRES
  connexion: "/connexion",
  changerPassowrd: "/changerPassowrd",
  passwordOublier: "/passwordOublier",
  updateCustom: "/updateCustom",
  choisirOffre: "/choisirOffre",
  getNotifications: "/getNotifications",
  updateNotifications: "/updateNotifications",
  getByCriteriaCustom: "/getByCriteriaCustom",
  updateContenuNotificationDispo: "/updateContenuNotificationDispo",
  editerSouscriptionEnseigne: "/editerSouscriptionEnseigne",

  // les pages

  pageAccueil: "/pages/dashboard",
  pageDashboardEnseigne: "/pages/tableau-de-bord-enseigne/",
  pageDashboard: "/pages/tableau-de-bord",
  pageLogin: "/authentification/login",
  pagePasswordOublier: "/authentification/password-oublier",

  pointDeVente: "pointDeVente",
  programmeFideliteTampon: "programmeFideliteTampon",
  programmeFideliteCarte: "programmeFideliteCarte",

  //size : 1 ,
  size: 5,

  // les etatInscripitionCode
  REFUSER: "REFUSER",
  ACCEPTER: "ACCEPTER",

  EN_COURS: "EN_COURS",
  AVENIR: "A venir",

  erreurDeConnexion: "Erreur de connexion. Veuillez reessayer !!!",
};
