import { Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import {
  NbGlobalPosition,
  NbGlobalPhysicalPosition,
  NbComponentStatus,
  NbToastrService,
} from "@nebular/theme";
import { NgForm } from "@angular/forms";

@Injectable({
  providedIn: "root",
})
export class UtilitiesService {
  constructor(private toastrService: NbToastrService, private router: Router) {}

  // debut notification *****************
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = "primary";
  destroyByClick = true;
  hasIcon = true;
  statusPrimary: NbComponentStatus = "primary";
  statusSuccess: NbComponentStatus = "success";
  statusWarning: NbComponentStatus = "warning";
  statusDanger: NbComponentStatus = "danger";
  statusInfo: NbComponentStatus = "info";

  // fin notification *****************

  connexionLocalStorage(userConnected) {
    if (userConnected != null) {
      localStorage.setItem(environment.user, userConnected.id);
      localStorage.setItem(environment.isAuth, "true");
      localStorage.setItem(environment.login, userConnected.login);
      localStorage.setItem(environment.nom, userConnected.nom);
    }
  }

  deconnexionLocalStorage() {
    localStorage.removeItem(environment.user);
    localStorage.removeItem(environment.isAuth);
    localStorage.removeItem(environment.login);
    localStorage.removeItem(environment.nom);
  }

  showToast(
    body: string,
    status: NbComponentStatus = this.statusPrimary,
    title: string = "retour"
  ) {
    //private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      //status: this.status,
      //status: this.status,
      status: status,
      destroyByClick: this.destroyByClick,
      duration: environment.TEMPS_TOAST,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `. ${title}` : "";

    //this.index += 1;
    this.toastrService.show(body, `Message ${titleContent}`, config);
  }

  resetForm(form: NgForm) {
    if (form != null) {
      form.resetForm();
    }
  }

  begin(content: string) {
    console.log("****************** BEGIN : ", content, "******************");
  }

  end(content: string) {
    console.log("****************** END : ", content, "******************");
  }
  get enseigneId() {
    return localStorage.getItem(environment.enseigneId);
  }
  get userId() {
    return localStorage.getItem(environment.user);
  }
  get loginUser() {
    return localStorage.getItem(environment.login);
  }
  get nomUser() {
    return localStorage.getItem(environment.nom);
  }
  isNotEmpty(datas: []) {
    return datas && datas.length > 0;
  }
  dateToString(date: any): string {
    //let date = new Date(this.dateDepot) ;
    console.log("date envoyé : ", date);
    return (
      date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear()
    );
  }
}
