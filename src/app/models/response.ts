export class Response<T> {
  constructor() {}
  item: T;
  items: Array<T>;
  hasError: boolean;
  status: {
    message: string;
    code: string;
  };
}
