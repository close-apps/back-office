import { Entreprise } from "./entreprise";

export class User {
  //constructor(){};

  id: any;
  villeId: number;
  enseigneId;
  roleId: number;
  continentId: number;
  paysId: number;
  compteurOffreNew: number;
  compteurOffreUpdate: number;
  totalCompteur: number;
  compteurAppelDoffresNew: number;
  compteurAppelDoffresSelect: number;
  compteurAppelDoffresUpdate: number;
  typeEntrepriseCode: any;
  dateFin;
  souscriptionId;
  programmeFideliteTamponId;
  pointDeVenteId;
  programmeFideliteCarteId;
  dateDebut;
  nom: string;
  titre: any;
  ordre: any;
  isSelected: any;
  token: any;
  code: string;
  libelle: string;
  prenom: string;
  email: string;
  telephone: string;
  login: string;
  password: string;
  newPassword: string;
  roleCode: string;
  test: any;
  dataEntreprise = new Entreprise();
  datasVille = new Array<User>();
  fonctionnalites: Array<any>;
  contenu: string;
  typeContenuStatiqueLibelle: string;
  typeContenuStatiqueCode: string;
  etatInscripitionCode: string;
  etatSouscriptionCode: string;

  paysLibelle: any;
  paysCode: any;

  datasSecteurActivite: any;
}
