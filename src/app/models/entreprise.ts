export class Entreprise {


    id:number;
    villeId:number;
    paysId : any ;
    typeEntrepriseId:number;
    statutJuridiqueId:number;
    nombreUser : number ;
    

    typeEntrepriseLibelle:string;
    paysLibelle:string;
    note : any ;
    classeNoteLibelle :string;
    statutJuridiqueLibelle:string;
    numeroDuns:string;
    numeroImmatriculation:string;
    nom:string;
    numeroFix:string;
    numeroFax:string;
    email:string;
    telephone:string;
    login:string;
    urlLogo:string;
    extensionLogo:string;
    name:string;
    extension:string;
    description:string;
    password:string;
    fichierBase64:string;
    typeEntrepriseCode : string ;
    isFichierOffre :boolean = false;
    villeLibelle :any ;
    cheminFichier : any ;
    urlFichier : any ;
    createdBy : any ;

    datasFichierPartenaire : any;
    datasFichierDescription : any;
    
    test : any ;

    fonctionnalites:Array<any>;

    //datasFichierDescription = [] ;
    datasPartenaire = [] ;
    datasSecteurDactiviteEntreprise = [];
    datasDomaineEntreprise : any;
    datasDomaine : any;
    datasSecteurDactivite = new Array<any>();
}
