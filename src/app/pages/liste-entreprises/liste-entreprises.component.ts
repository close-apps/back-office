import { Component, OnInit } from "@angular/core";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-liste-entreprises",
  templateUrl: "./liste-entreprises.component.html",
  styleUrls: ["./liste-entreprises.component.scss"],
})
export class ListeEntreprisesComponent implements OnInit {
  constructor(
    private metiers: MetiersService,
    private utilitiesService: UtilitiesService
  ) {}

  itemsEnseigne: any;

  ngOnInit() {
    this.initDatas();
  }

  initDatas() {
    this.metiers.getByCriteria(environment.enseigne).subscribe(
      (res) => {
        let response = new Response<any>();
        response = res;

        if (!response.hasError) {
          this.itemsEnseigne = response.items;
        } else {
        }
      },
      (error) => {
      }
    );
  }
  validerInscription(data: any) {
    this.utilitiesService.begin("validerInscription");

    let model = new User();
    model.id = data.id;
    model.etatInscripitionCode = environment.ACCEPTER;

    this.metiers.update(environment.enseigne, [model]).subscribe(
      (res) => {
        let response = new Response<any>();
        response = res;
        if (!response.hasError) {
          this.itemsEnseigne = response.items;

          // notification
          this.utilitiesService.showToast(
            response.status.message,
            this.utilitiesService.statusSuccess
          );

          // refresh
          this.initDatas();
        } else {
          this.utilitiesService.showToast(
            response.status.message,
            this.utilitiesService.statusDanger
          );
        }
      },
      (error) => {
        this.utilitiesService.showToast(
          environment.erreurDeConnexion,
          this.utilitiesService.statusInfo
        );
      }
    );
  }

  refuserInscription(data: any) {
    this.utilitiesService.begin("refuserInscription");

    let model = new User();
    model.id = data.id;
    model.etatInscripitionCode = environment.REFUSER;

    this.metiers.update(environment.enseigne, [model]).subscribe(
      (res) => {
        let response = new Response<any>();
        response = res;
        if (!response.hasError) {
          this.itemsEnseigne = response.items;

          // notification
          this.utilitiesService.showToast(
            response.status.message,
            this.utilitiesService.statusSuccess
          );

          // refresh
          this.initDatas();
        } else {
          this.utilitiesService.showToast(
            response.status.message,
            this.utilitiesService.statusDanger
          );
        }
      },
      (error) => {
        this.utilitiesService.showToast(
          environment.erreurDeConnexion,
          this.utilitiesService.statusInfo
        );
      }
    );
  }
}
