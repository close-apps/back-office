import { NgModule } from "@angular/core";
import {
  NbButtonModule,
  NbCardModule,
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbIconModule,
  NbInputModule,
  NbMenuModule,
  NbSelectModule,
  NbSpinnerModule,
  NbToastrModule,
  NbWindowModule,
} from "@nebular/theme";

import { ThemeModule } from "../@theme/theme.module";
import { PagesComponent } from "./pages.component";
import { DashboardModule } from "./dashboard/dashboard.module";
import { ECommerceModule } from "./e-commerce/e-commerce.module";
import { PagesRoutingModule } from "./pages-routing.module";
import { MiscellaneousModule } from "./miscellaneous/miscellaneous.module";
import { ListeEntreprisesComponent } from "./liste-entreprises/liste-entreprises.component";
import { TableauDeBordComponent } from "./tableau-de-bord/tableau-de-bord.component";
import { TableauDeBordEnseigneComponent } from "./tableau-de-bord-enseigne/tableau-de-bord-enseigne.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxEchartsModule } from "ngx-echarts";
import { ChartModule } from "angular2-chartjs";
import { SouscriptionsComponent } from "./souscriptions/souscriptions.component";

@NgModule({
  imports: [
    ReactiveFormsModule,
    FormsModule,
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    NbCardModule,
    NbIconModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbDatepickerModule,
    NgxEchartsModule, // pour les graphes
    ChartModule, // pour les graphes

    // ajouter
    NbSpinnerModule, // pour les loading lors des attentes
  ],
  declarations: [
    PagesComponent,
    ListeEntreprisesComponent,
    TableauDeBordComponent,
    TableauDeBordEnseigneComponent,
    SouscriptionsComponent,
  ],
})
export class PagesModule {}
