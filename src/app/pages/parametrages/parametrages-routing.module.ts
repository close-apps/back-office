import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SecteurActivitesComponent } from "./secteur-activites/secteur-activites.component";
import { SouscriptionComponent } from "./souscription/souscription.component";
import { PallierForfaitComponent } from "./pallier-forfait/pallier-forfait.component";
import { DelaiForfaitComponent } from "./delai-forfait/delai-forfait.component";
import { NotFoundComponent } from "../miscellaneous/not-found/not-found.component";
import { ParametragesComponent } from "./parametrages.component";
import { RoleComponent } from "./role/role.component";
import { TypeRegleSecuriteTamponComponent } from "./type-regle-securite-tampon/type-regle-securite-tampon.component";
import { TypeRegleSecuriteCarteComponent } from "./type-regle-securite-carte/type-regle-securite-carte.component";
import { UniteTempsComponent } from './unite-temps/unite-temps.component';
import { TypeActionComponent } from './type-action/type-action.component';

const routes: Routes = [
  {
    path: "",
    component: ParametragesComponent,
    children: [
      {
        path: "duree-forfait",
        component: DelaiForfaitComponent,
      },
      {
        path: "pallier-forfait",
        component: PallierForfaitComponent,
      },
      {
        path: "souscription",
        component: SouscriptionComponent,
      },
      {
        path: "role",
        component: RoleComponent,
      },
      {
        path: "secteur-activite",
        component: SecteurActivitesComponent,
      },
      {
        path: "unite-temps",
        component: UniteTempsComponent,
      },
      {
        path: "type-action",
        component: TypeActionComponent,
      },
      {
        path: "regle-securite-carte",
        component: TypeRegleSecuriteCarteComponent,
      },
      {
        path: "regle-securite-tampon",
        component: TypeRegleSecuriteTamponComponent,
      },

      /*{
        path: "**",
        component: NotFoundComponent,
      },*/
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParametragesRoutingModule {}
