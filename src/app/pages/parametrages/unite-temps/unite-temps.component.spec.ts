import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniteTempsComponent } from './unite-temps.component';

describe('UniteTempsComponent', () => {
  let component: UniteTempsComponent;
  let fixture: ComponentFixture<UniteTempsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniteTempsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniteTempsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
