import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeRegleSecuriteTamponComponent } from './type-regle-securite-tampon.component';

describe('TypeRegleSecuriteTamponComponent', () => {
  let component: TypeRegleSecuriteTamponComponent;
  let fixture: ComponentFixture<TypeRegleSecuriteTamponComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeRegleSecuriteTamponComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeRegleSecuriteTamponComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
