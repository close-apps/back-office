import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DelaiForfaitComponent } from './delai-forfait.component';

describe('DelaiForfaitComponent', () => {
  let component: DelaiForfaitComponent;
  let fixture: ComponentFixture<DelaiForfaitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DelaiForfaitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelaiForfaitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
