import { Component, OnInit } from "@angular/core";
import { environment } from "../../../../environments/environment";
import { MetiersService } from "../../../services/metiers.service";
import { ParametragesService } from "../../../services/parametrages.service";
import { UtilitiesService } from "../../../services/utilities.service";

@Component({
  selector: "ngx-role",
  templateUrl: "./role.component.html",
  styleUrls: ["./role.component.scss"],
})
export class RoleComponent implements OnInit {
  constructor(
    private parametrages: ParametragesService,
    private metiers: MetiersService,
    private utilities: UtilitiesService
  ) {}

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: "Id",
        type: "number",
        editable: false,
        addable: false,
      },
      code: {
        title: "Code",
        type: "string",
      },
      libelle: {
        title: "Libelle",
        type: "string",
      },
    },
  };

  //
  /*
    settings = {
      add: {
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true,

      },
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true,

      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
      columns: {
        code: {
          title: 'Matricule',
          type: 'string',
          editable : false,
          addable: false,

        },
        fermeId: {
          title: 'Ferme',
          type: 'number',
          editor: {
          type: 'list',
          config: {
            //selectText: 'Select',
            list: []
          },
          }
        },
        typeAnimalId: {
          title: 'Animal',
          type: 'number',
          editor: {
          type: 'list',
          config: {
            //selectText: 'Select',
            list: []
          },
          }
        },
        sexeId: {
          title: 'Sexe',
          type: 'number',
          editor: {
          type: 'list',
          config: {
            //selectText: 'Select',
            list: []
          },
          }
        },
        etatLibelle: {
          title: 'Last Name',
          type: 'string',
        },
      },
    };
  //*/
  //source: LocalDataSource = new LocalDataSource();

  source: any;
  //data : any;

  onDeleteConfirm(event): void {
    if (window.confirm("Are you sure you want to delete?")) {
      event.confirm.resolve();

      let datas = new Array<any>();

      datas.push(event.data);
      this.metiers.delete(environment.role, datas).subscribe(
        (res) => {
          if (!res["hasError"]) {
            // setter le list de settings
            this.utilities.showToast(
              res["status"].message,
              this.utilities.statusSuccess
            );

            this.refresh();
          } else {
            this.utilities.showToast(
              res["status"].message,
              this.utilities.statusDanger
            );
          }
        },
        (error) => {
          this.utilities.showToast(
            environment.erreurDeConnexion,
            this.utilities.statusInfo
          );
        }
      );
    } else {
      event.confirm.reject();
    }
    //this.source = this.data ;
  }

  onCreateConfirm(event) {
    let datas = new Array<any>();

    datas.push(event.newData);
    this.metiers.create(environment.role, datas).subscribe(
      (res) => {
        if (!res["hasError"]) {
          // setter le list de settings
          this.utilities.showToast(
            res["status"].message,
            this.utilities.statusSuccess
          );

          this.refresh();
        } else {
          this.utilities.showToast(
            res["status"].message,
            this.utilities.statusDanger
          );
        }
      },
      (error) => {
        this.utilities.showToast(
          environment.erreurDeConnexion,
          this.utilities.statusInfo
        );
      }
    );
  }

  onSaveConfirm(event) {
    let datas = new Array<any>();

    datas.push(event.newData);
    this.metiers.update(environment.role, datas).subscribe(
      (res) => {
        if (!res["hasError"]) {
          // setter le list de settings
          this.utilities.showToast(
            res["status"].message,
            this.utilities.statusSuccess
          );
          this.refresh();
        } else {
          this.utilities.showToast(
            res["status"].message,
            this.utilities.statusDanger
          );
        }
      },
      (error) => {
        this.utilities.showToast(
          environment.erreurDeConnexion,
          this.utilities.statusInfo
        );
      }
    );
  }

  itemsSource: any;
  ngOnInit() {
    // recuperation des listes deroulantes
    this.refresh();
  }

  refresh() {
    this.metiers.getByCriteria(environment.role).subscribe(
      (res) => {
        if (!res["hasError"]) {
          this.itemsSource = res["items"];

          // setter le list de settings
          this.source = this.itemsSource;
        } else {
        }
      },
      (error) => {
        this.utilities.showToast(
          environment.erreurDeConnexion,
          this.utilities.statusInfo
        );
      }
    );
  }
}
