import { Component, OnInit } from "@angular/core";
import { ParametragesService } from "../../../services/parametrages.service";
import { MetiersService } from "../../../services/metiers.service";
import { UtilitiesService } from "../../../services/utilities.service";
import { environment } from "../../../../environments/environment";
import { Response } from "../../../models/response";

@Component({
  selector: "ngx-souscription",
  templateUrl: "./souscription.component.html",
  styleUrls: ["./souscription.component.scss"],
})
export class SouscriptionComponent implements OnInit {
  constructor(
    private parametrages: ParametragesService,
    private metiers: MetiersService,
    private utilities: UtilitiesService
  ) {}

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: "Id",
        type: "number",
        editable: false,
        addable: false,
      },
      dureeForfaitLibelle: {
        title: "Libelle forfait",
        type: "string",
        editor: {
          type: "list",
          config: {
            //selectText: 'Select',
            list: [],
          },
        },
      },
      pallierForfaitLibelle: {
        title: "Pallier forfait",
        type: "string",
        editor: {
          type: "list",
          config: {
            //selectText: 'Select',
            list: [],
          },
        },
      },
      prix: {
        title: "Prix",
        type: "string",
      },
      nbreMaxiUser: { title: "Nbre maxi user", type: "number" },
      description: {
        title: "Description",
        type: "string",
      },
    },
  };

  //
  /*
    settings = {
      add: {
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true,

      },
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true,

      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
      columns: {
        code: {
          title: 'Matricule',
          type: 'string',
          editable : false,
          addable: false,

        },
        fermeId: {
          title: 'Ferme',
          type: 'number',
          editor: {
          type: 'list',
          config: {
            //selectText: 'Select',
            list: []
          },
          }
        },
        typeAnimalId: {
          title: 'Animal',
          type: 'number',
          editor: {
          type: 'list',
          config: {
            //selectText: 'Select',
            list: []
          },
          }
        },
        sexeId: {
          title: 'Sexe',
          type: 'number',
          editor: {
          type: 'list',
          config: {
            //selectText: 'Select',
            list: []
          },
          }
        },
        etatLibelle: {
          title: 'Last Name',
          type: 'string',
        },
      },
    };
  //*/
  //source: LocalDataSource = new LocalDataSource();

  source: any;
  //data : any;

  onDeleteConfirm(event): void {
    if (window.confirm("Are you sure you want to delete?")) {
      event.confirm.resolve();

      let datas = new Array<any>();

      datas.push(event.data);
      this.metiers.delete(environment.souscription, datas).subscribe(
        (res) => {
          if (!res["hasError"]) {
            // setter le list de settings
            this.utilities.showToast(
              res["status"].message,
              this.utilities.statusSuccess
            );

            this.refresh();
          } else {
            this.utilities.showToast(
              res["status"].message,
              this.utilities.statusDanger
            );
          }
        },
        (error) => {
          this.utilities.showToast(
            environment.erreurDeConnexion,
            this.utilities.statusInfo
          );
        }
      );
    } else {
      event.confirm.reject();
    }
    //this.source = this.data ;
  }

  onCreateConfirm(event) {
    let datas = new Array<any>();

    datas.push(event.newData);
    this.metiers.create(environment.souscription, datas).subscribe(
      (res) => {
        if (!res["hasError"]) {
          // setter le list de settings
          this.utilities.showToast(
            res["status"].message,
            this.utilities.statusSuccess
          );

          this.refresh();
        } else {
          this.utilities.showToast(
            res["status"].message,
            this.utilities.statusDanger
          );
        }
      },
      (error) => {
        this.utilities.showToast(
          environment.erreurDeConnexion,
          this.utilities.statusInfo
        );
      }
    );

    // by melissa
    //this.data.push(event.newData);

    //console.log( " this.data" ,this.data);
  }

  onSaveConfirm(event) {
    //this.source = this.data ;

    let datas = new Array<any>();

    datas.push(event.newData);
    this.metiers.update(environment.souscription, datas).subscribe(
      (res) => {
        if (!res["hasError"]) {
          // setter le list de settings
          this.utilities.showToast(
            res["status"].message,
            this.utilities.statusSuccess
          );
          this.refresh();
        } else {
          this.utilities.showToast(
            res["status"].message,
            this.utilities.statusDanger
          );
        }
      },
      (error) => {
        this.utilities.showToast(
          environment.erreurDeConnexion,
          this.utilities.statusInfo
        );
      }
    );
  }
  itemsFerme: any;
  itemsSexe: any;
  itemsEtat: any;
  itemsAnimaux: any;
  itemsTypeAnimal: any;

  itemsDureeForfait: any;
  itemsPallierForfait: any;
  items: any;
  ngOnInit() {
    // recuperation des listes deroulantes
    this.refresh();
  }

  refresh() {
    this.metiers.getByCriteria(environment.souscription).subscribe(
      (res) => {
        let response = new Response<any>();
        response = res;
        if (!response.hasError) {
          this.items = response.items;

          // setter le list de settings
          this.source = this.items;

          this.metiers.getByCriteria(environment.dureeForfait).subscribe(
            (res) => {
              if (!res["hasError"]) {
                this.itemsDureeForfait = res["items"];

                // setter le list de settings
                //this.settings.columns.paysLibelle.editor.config.list = this.itemsPays ;

                this.settings.columns.dureeForfaitLibelle.editor.config.list =
                  this.itemsDureeForfait.map((item) => {
                    return { value: item.libelle, title: item.libelle };
                  });
                this.settings = Object.assign({}, this.settings);
              } else {
              }
            },
            (error) => {
              this.utilities.showToast(
                environment.erreurDeConnexion,
                this.utilities.statusInfo
              );
            }
          );

          this.metiers.getByCriteria(environment.pallierForfait).subscribe(
            (res) => {
              if (!res["hasError"]) {
                this.itemsPallierForfait = res["items"];

                // setter le list de settings
                //this.settings.columns.paysLibelle.editor.config.list = this.itemsPays ;

                this.settings.columns.pallierForfaitLibelle.editor.config.list =
                  this.itemsPallierForfait.map((item) => {
                    return { value: item.libelle, title: item.libelle };
                  });
                this.settings = Object.assign({}, this.settings);
              } else {
              }
            },
            (error) => {
              this.utilities.showToast(
                environment.erreurDeConnexion,
                this.utilities.statusInfo
              );
            }
          );
        } else {
        }
      },
      (error) => {
        this.utilities.showToast(
          environment.erreurDeConnexion,
          this.utilities.statusInfo
        );
      }
    );
  }
}
