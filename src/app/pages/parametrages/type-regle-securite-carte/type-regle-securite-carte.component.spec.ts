import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeRegleSecuriteCarteComponent } from './type-regle-securite-carte.component';

describe('TypeRegleSecuriteCarteComponent', () => {
  let component: TypeRegleSecuriteCarteComponent;
  let fixture: ComponentFixture<TypeRegleSecuriteCarteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeRegleSecuriteCarteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeRegleSecuriteCarteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
