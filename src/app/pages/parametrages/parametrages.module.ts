import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ParametragesRoutingModule } from "./parametrages-routing.module";
import { ParametragesComponent } from "./parametrages.component";
import { PallierForfaitComponent } from "./pallier-forfait/pallier-forfait.component";
import { DelaiForfaitComponent } from "./delai-forfait/delai-forfait.component";
import { SouscriptionComponent } from "./souscription/souscription.component";
import { SecteurActivitesComponent } from "./secteur-activites/secteur-activites.component";
import { Ng2SmartTableModule } from "ng2-smart-table";
import { MiscellaneousModule } from "../miscellaneous/miscellaneous.module";
import { NbCardModule } from "@nebular/theme";
import { RoleComponent } from './role/role.component';
import { UniteTempsComponent } from './unite-temps/unite-temps.component';
import { TypeRegleSecuriteTamponComponent } from './type-regle-securite-tampon/type-regle-securite-tampon.component';
import { TypeRegleSecuriteCarteComponent } from './type-regle-securite-carte/type-regle-securite-carte.component';
import { TypeActionComponent } from './type-action/type-action.component';

@NgModule({
  declarations: [
    ParametragesComponent,
    PallierForfaitComponent,
    DelaiForfaitComponent,
    SouscriptionComponent,
    SecteurActivitesComponent,
    RoleComponent,
    UniteTempsComponent,
    TypeRegleSecuriteTamponComponent,
    TypeRegleSecuriteCarteComponent,
    TypeActionComponent,
  ],
  imports: [
    CommonModule,
    ParametragesRoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
    MiscellaneousModule,
  ],
})
export class ParametragesModule {}
