import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PallierForfaitComponent } from './pallier-forfait.component';

describe('PallierForfaitComponent', () => {
  let component: PallierForfaitComponent;
  let fixture: ComponentFixture<PallierForfaitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PallierForfaitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PallierForfaitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
