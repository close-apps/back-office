import { Component, OnInit } from "@angular/core";
import { ParametragesService } from "../../../services/parametrages.service";
import { MetiersService } from "../../../services/metiers.service";
import { UtilitiesService } from "../../../services/utilities.service";
import { environment } from "../../../../environments/environment";

@Component({
  selector: "ngx-pallier-forfait",
  templateUrl: "./pallier-forfait.component.html",
  styleUrls: ["./pallier-forfait.component.scss"],
})
export class PallierForfaitComponent implements OnInit {
  constructor(
    private parametrages: ParametragesService,
    private metiers: MetiersService,
    private utilities: UtilitiesService
  ) {}

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: "Id",
        type: "number",
        editable: false,
        addable: false,
      },
      code: {
        title: "Code",
        type: "string",
      },
      libelle: {
        title: "Libelle",
        type: "string",
      },
      description: {
        title: "Description",
        type: "string",
      },
    },
  };

  //
  /*
    settings = {
      add: {
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true,

      },
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true,

      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
      columns: {
        code: {
          title: 'Matricule',
          type: 'string',
          editable : false,
          addable: false,

        },
        fermeId: {
          title: 'Ferme',
          type: 'number',
          editor: {
          type: 'list',
          config: {
            //selectText: 'Select',
            list: []
          },
          }
        },
        typeAnimalId: {
          title: 'Animal',
          type: 'number',
          editor: {
          type: 'list',
          config: {
            //selectText: 'Select',
            list: []
          },
          }
        },
        sexeId: {
          title: 'Sexe',
          type: 'number',
          editor: {
          type: 'list',
          config: {
            //selectText: 'Select',
            list: []
          },
          }
        },
        etatLibelle: {
          title: 'Last Name',
          type: 'string',
        },
      },
    };
  //*/
  //source: LocalDataSource = new LocalDataSource();

  source: any;
  //data : any;

  onDeleteConfirm(event): void {
    if (window.confirm("Are you sure you want to delete?")) {
      event.confirm.resolve();

      let datas = new Array<any>();

      datas.push(event.data);
      this.metiers.delete(environment.pallierForfait, datas).subscribe(
        (res) => {
          if (!res["hasError"]) {
            // setter le list de settings
            this.utilities.showToast(
              res["status"].message,
              this.utilities.statusSuccess
            );

            this.refresh();
          } else {
            this.utilities.showToast(
              res["status"].message,
              this.utilities.statusDanger
            );
          }
        },
        (error) => {
          this.utilities.showToast(
            environment.erreurDeConnexion,
            this.utilities.statusInfo
          );
        }
      );
    } else {
      event.confirm.reject();
    }
    //this.source = this.data ;
  }

  onCreateConfirm(event) {
    let datas = new Array<any>();

    datas.push(event.newData);
    this.metiers.create(environment.pallierForfait, datas).subscribe(
      (res) => {
        if (!res["hasError"]) {
          // setter le list de settings
          this.utilities.showToast(
            res["status"].message,
            this.utilities.statusSuccess
          );

          this.refresh();
        } else {
          this.utilities.showToast(
            res["status"].message,
            this.utilities.statusDanger
          );
        }
      },
      (error) => {
        this.utilities.showToast(
          environment.erreurDeConnexion,
          this.utilities.statusInfo
        );
      }
    );

    // by melissa
    //this.data.push(event.newData);

    //console.log( " this.data" ,this.data);
  }

  onSaveConfirm(event) {
    //this.source = this.data ;
    let datas = new Array<any>();

    datas.push(event.newData);
    this.metiers.update(environment.pallierForfait, datas).subscribe(
      (res) => {
        if (!res["hasError"]) {
          // setter le list de settings
          this.utilities.showToast(
            res["status"].message,
            this.utilities.statusSuccess
          );
          this.refresh();
        } else {
          this.utilities.showToast(
            res["status"].message,
            this.utilities.statusDanger
          );
        }
      },
      (error) => {
        this.utilities.showToast(
          environment.erreurDeConnexion,
          this.utilities.statusInfo
        );
      }
    );
  }

  itemsSource: any;
  ngOnInit() {
    // recuperation des listes deroulantes
    this.refresh();
  }

  refresh() {
    this.metiers.getByCriteria(environment.pallierForfait).subscribe(
      (res) => {
        if (!res["hasError"]) {
          this.itemsSource = res["items"];

          // setter le list de settings
          this.source = this.itemsSource;
        } else {
        }
      },
      (error) => {
        this.utilities.showToast(
          environment.erreurDeConnexion,
          this.utilities.statusInfo
        );
      }
    );
  }
}
