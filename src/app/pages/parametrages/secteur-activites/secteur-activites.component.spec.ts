import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecteurActivitesComponent } from './secteur-activites.component';

describe('SecteurActivitesComponent', () => {
  let component: SecteurActivitesComponent;
  let fixture: ComponentFixture<SecteurActivitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecteurActivitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecteurActivitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
