import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NbColorHelper, NbThemeService } from "@nebular/theme";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-tableau-de-bord",
  templateUrl: "./tableau-de-bord.component.html",
  styleUrls: ["./tableau-de-bord.component.scss"],
})
export class TableauDeBordComponent implements OnInit {
  isBasicStats = false;
  dateModel;

  pdfTampon;
  pdfCarte;
  pointDeVente;
  enseigne;
  itemStats;
  optionsGain;
  dataGain;
  dataEnseigne;
  optionsEnseigne;

  souscription;

  datasSouscription;
  dataContratArreter;
  optionsContratArreter;

  optionsInscritsParPdfTampon;
  themeSubscription: any;
  optionsUser;
  currentDateFin = new Date();
  currentDateDebut = new Date();

  constructor(
    private theme: NbThemeService,
    private utilitiesService: UtilitiesService,
    private metiersService: MetiersService,
    private router: Router
  ) {}
  ngOnInit() {
    let data = new User();

    this.currentDateDebut.setFullYear(this.currentDateDebut.getFullYear() - 1); // un an en arrière
    this.currentDateDebut.setDate(this.currentDateDebut.getDate() + 1); // ajout d'un jour

    this.dateModel = {
      start: this.currentDateDebut,
      end: this.currentDateFin,
    };

    data.id = this.utilitiesService.enseigneId;
    this.metiersService.getBasicStat(environment.enseigne, data).subscribe(
      (res) => {
        let response = new Response<any>();
        response = res;
        if (response != null && !response.hasError) {
          this.enseigne = response.items[0];
        }
      },
      (err) => {
        this.utilitiesService.showToast(
          environment.erreurDeConnexion,
          this.utilitiesService.statusInfo
        );
      }
    );

    let dataSouscription = new User();
    this.metiersService
      .getByCriteriaForStats(environment.souscription, dataSouscription)
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (response != null && !response.hasError) {
            this.datasSouscription = response.items;
          }
        },
        (err) => {
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
        }
      );
    this.getStats();
  }

  getStats(
    dateDebut = this.utilitiesService.dateToString(this.currentDateDebut),
    dateFin = this.utilitiesService.dateToString(this.currentDateFin)
  ) {
    let dataStats = new User();

    dataStats.id = this.utilitiesService.enseigneId;
    dataStats.dateDebut = dateDebut;
    dataStats.dateFin = dateFin;
    if (this.souscription) {
      dataStats.souscriptionId = this.souscription.id;
    }

    this.metiersService.getStats(environment.user, dataStats).subscribe(
      (res) => {
        let response = new Response<any>();
        response = res;
        if (response != null && !response.hasError) {
          if (response.items && response.items.length > 0) {
            this.itemStats = response.items[0];

            // mettre la logique

            this.themeSubscription = this.theme
              .getJsTheme()
              .subscribe((config) => {
                const colors: any = config.variables;
                const chartjs: any = config.variables.chartjs;
                const echarts: any = config.variables.echarts;

                this.optionsInscritsParPdfTampon = {
                  backgroundColor: echarts.bg,
                  color: [
                    colors.warningLight,
                    colors.infoLight,
                    colors.dangerLight,
                    colors.successLight,
                    colors.primaryLight,
                  ],
                  tooltip: {
                    trigger: "item",
                    formatter: "{a} <br/>{b} : {c} ({d}%)",
                  },
                  legend: {
                    orient: "vertical",
                    left: "left",
                    //data: ["USA", "Germany", "France", "Canada", "Russia"],
                    data: this.itemStats.legendPdfTampon,
                    textStyle: {
                      color: echarts.textColor,
                    },
                  },
                  series: [
                    {
                      name: "Inscrits",
                      type: "pie",
                      radius: "80%",
                      center: ["50%", "50%"],
                      //  data: [
                      //    { value: 335, name: "Germany" },
                      //    { value: 310, name: "France" },
                      //    { value: 234, name: "Canada" },
                      //    { value: 135, name: "Russia" },
                      //    { value: 1548, name: "USA" },
                      //  ],
                      data: this.itemStats.inscritParPdfTampon,
                      itemStyle: {
                        emphasis: {
                          shadowBlur: 10,
                          shadowOffsetX: 0,
                          shadowColor: echarts.itemHoverShadowColor,
                        },
                      },
                      label: {
                        normal: {
                          textStyle: {
                            color: echarts.textColor,
                          },
                        },
                      },
                      labelLine: {
                        normal: {
                          lineStyle: {
                            color: echarts.axisLineColor,
                          },
                        },
                      },
                    },
                  ],
                };

                this.dataEnseigne = {
                  labels: this.itemStats.abscisseGraphe,
                  datasets: [
                    {
                      data: this.itemStats.nbreEnseigne,
                      label: "Nbre Enseigne",
                      backgroundColor: NbColorHelper.hexToRgbA(
                        colors.primaryLight,
                        0.8
                      ),
                    },
                    {
                      //data: [65, 59, 80, 81, 56, 55, 40],
                      data: this.itemStats.nbreNewEnseigne,
                      label: "Nouvel Enseigne",
                      backgroundColor: NbColorHelper.hexToRgbA(
                        colors.infoLight,
                        0.8
                      ),
                    },
                  ],
                };
                this.optionsEnseigne = {
                  maintainAspectRatio: false,
                  responsive: true,
                  legend: {
                    labels: {
                      fontColor: chartjs.textColor,
                    },
                  },
                  scales: {
                    xAxes: [
                      {
                        gridLines: {
                          display: false,
                          color: chartjs.axisLineColor,
                        },
                        ticks: {
                          fontColor: chartjs.textColor,
                        },
                      },
                    ],
                    yAxes: [
                      {
                        gridLines: {
                          display: true,
                          color: chartjs.axisLineColor,
                        },
                        ticks: {
                          fontColor: chartjs.textColor,
                        },
                      },
                    ],
                  },
                };

                this.dataContratArreter = {
                  labels: this.itemStats.abscisseGraphe,
                  datasets: [
                    {
                      //data: [65, 59, 80, 81, 56, 55, 40],
                      data: this.itemStats.nbreContratArreter,
                      label: "Nbre enseigne par contrat",
                      backgroundColor: NbColorHelper.hexToRgbA(
                        colors.primaryLight,
                        0.8
                      ),
                    },
                    {
                      //data: [65, 59, 80, 81, 56, 55, 40],
                      data: this.itemStats.nbreEnseigneParContrat,
                      label: "Programme à tampons",
                      backgroundColor: NbColorHelper.hexToRgbA(
                        colors.infoLight,
                        0.8
                      ),
                    },
                  ],
                };
                this.optionsContratArreter = {
                  maintainAspectRatio: false,
                  responsive: true,
                  legend: {
                    labels: {
                      fontColor: chartjs.textColor,
                    },
                  },
                  scales: {
                    xAxes: [
                      {
                        gridLines: {
                          display: false,
                          color: chartjs.axisLineColor,
                        },
                        ticks: {
                          fontColor: chartjs.textColor,
                        },
                      },
                    ],
                    yAxes: [
                      {
                        gridLines: {
                          display: true,
                          color: chartjs.axisLineColor,
                        },
                        ticks: {
                          fontColor: chartjs.textColor,
                        },
                      },
                    ],
                  },
                };

                this.dataGain = {
                  labels: this.itemStats.abscisseGraphe,
                  datasets: [
                    {
                      //data: [65, 59, 80, 81, 56, 55, 40],
                      data: this.itemStats.gain,
                      label: "Gains",
                      backgroundColor: NbColorHelper.hexToRgbA(
                        colors.primaryLight,
                        0.8
                      ),
                    },
                  ],
                };
                this.optionsGain = {
                  maintainAspectRatio: false,
                  responsive: true,
                  legend: {
                    labels: {
                      fontColor: chartjs.textColor,
                    },
                  },
                  scales: {
                    xAxes: [
                      {
                        gridLines: {
                          display: false,
                          color: chartjs.axisLineColor,
                        },
                        ticks: {
                          fontColor: chartjs.textColor,
                        },
                      },
                    ],
                    yAxes: [
                      {
                        gridLines: {
                          display: true,
                          color: chartjs.axisLineColor,
                        },
                        ticks: {
                          fontColor: chartjs.textColor,
                        },
                      },
                    ],
                  },
                };
              });
          }
        }
      },
      (err) => {
        this.utilitiesService.showToast(
          environment.erreurDeConnexion,
          this.utilitiesService.statusInfo
        );
      }
    );
  }
  changeDebut(date) {
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }

  setIsBasicStats() {
    this.isBasicStats = !this.isBasicStats;
  }

  search() {
    this.getStats(
      this.utilitiesService.dateToString(this.dateModel.start),
      this.utilitiesService.dateToString(this.dateModel.end)
    );
  }

  voir(data) {
    this.router.navigateByUrl(environment.pageDashboardEnseigne + data.id);
  }
  bloquer(data) {
    this.utilitiesService.showToast(
      environment.AVENIR,
      this.utilitiesService.statusInfo
    );
  }
}
