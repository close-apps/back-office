import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { Component, OnInit } from "@angular/core";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-souscriptions",
  templateUrl: "./souscriptions.component.html",
  styleUrls: ["./souscriptions.component.scss"],
})
export class SouscriptionsComponent implements OnInit {
  constructor(
    private metiers: MetiersService,
    private utilitiesService: UtilitiesService
  ) {}

  itemsHistoriqueSouscriptionEnseigne: any;
  datasSouscription;
  isAcceuil = true;
  loadingSpinner = false;
  currentOffreId = 1;
  isSubmitted = false;

  ngOnInit() {
    this.initDatas();
  }

  initDatas() {
    this.metiers
      .getByCriteria(environment.historiqueSouscriptionEnseigne)
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (!response.hasError) {
            this.itemsHistoriqueSouscriptionEnseigne = response.items;
          } else {
          }
        },
        (error) => {}
      );
  }

  validerSouscription(data: any) {
    this.utilitiesService.begin("validerSouscription");

    let model = new User();
    model.id = data.id;
    model.etatSouscriptionCode = environment.ACCEPTER;

    this.metiers
      .editerSouscriptionEnseigne(
        environment.historiqueSouscriptionEnseigne,
        model
      )
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (!response.hasError) {
            // this.itemsHistoriqueSouscriptionEnseigne = response.items;

            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );

            // refresh
            this.initDatas();
          } else {
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusDanger
            );
          }
        },
        (error) => {
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
        }
      );
  }

  refuserSouscription(data: any) {
    this.utilitiesService.begin("refuserInscription");

    let model = new User();
    model.id = data.id;
    model.etatSouscriptionCode = environment.REFUSER;

    this.metiers
      .editerSouscriptionEnseigne(
        environment.historiqueSouscriptionEnseigne,
        model
      )
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (!response.hasError) {
            // this.itemsHistoriqueSouscriptionEnseigne = response.items;

            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );

            // refresh
            this.initDatas();
          } else {
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusDanger
            );
          }
        },
        (error) => {
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
        }
      );
  }

  editerSouscription(data) {
    console.log("data ::: ", data);
    this.isAcceuil = false;
    this.model = Object.assign({}, data);

    this.currentOffreId = data.souscriptionId;
    this.currentOffre = Object.assign({}, data);
    this.currentOffre.id = this.currentOffreId;

    if (data.souscriptionDescription) {
      this.currentOffre.description = data.souscriptionDescription;
    }

    this.metiers.getByCriteria(environment.souscription).subscribe(
      (res) => {
        let response = new Response<any>();
        response = res;
        if (!response.hasError) {
          this.datasSouscription = response.items;
        } else {
        }
      },
      (error) => {}
    );
    this.utilitiesService.begin("editerSouscription");
  }

  model: any;
  currentOffre;

  changeOffre(event) {
    console.log("event ::: ", event);

    for (let index = 0; index < this.datasSouscription.length; index++) {
      if (event == this.datasSouscription[index].id) {
        this.currentOffre = this.datasSouscription[index];
        this.model.prixSpecifique = isNaN(this.datasSouscription[index].prix)
          ? null
          : this.datasSouscription[index].prix;
        break;
      }
    }
  }

  submit() {
    this.loadingSpinner = true;
    this.isSubmitted = true; // disable button submit

    this.model.souscriptionId = this.currentOffre.id;
    //this.model.id = this.currentOffre.id;

    this.metiers
      .update(environment.historiqueSouscriptionEnseigne, [this.model])
      .subscribe(
        (res) => {
          this.loadingSpinner = false;

          let response = new Response<any>();
          response = res;
          if (!response.hasError) {
            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );

            // refresh
            this.initDatas();
            this.isAcceuil = true;
          } else {
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusDanger
            );
            // activer le button du formulaire
            this.isSubmitted = false;
          }
        },
        (error) => {
          this.loadingSpinner = false;

          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
          this.isSubmitted = false;
        }
      );
  }
}
