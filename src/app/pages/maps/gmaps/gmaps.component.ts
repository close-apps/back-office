import { Component } from "@angular/core";

@Component({
  selector: "ngx-gmaps",
  styleUrls: ["./gmaps.component.scss"],
  template: `
    <nb-card>
      <nb-card-header>Google Maps</nb-card-header>
      <nb-card-body>
        <agm-map [latitude]="lat" [longitude]="lng">
          <agm-marker [latitude]="lat" [longitude]="lng"></agm-marker>
        </agm-map>
      </nb-card-body>
    </nb-card>
  `,
})
export class GmapsComponent {
  lat = 5.46712;
  lng = -4.030724;
  latOld = 51.678418;
  lngOld = 7.809007;
}
