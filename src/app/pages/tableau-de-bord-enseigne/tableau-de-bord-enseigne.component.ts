import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { NbColorHelper, NbThemeService } from "@nebular/theme";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-tableau-de-bord-enseigne",
  templateUrl: "./tableau-de-bord-enseigne.component.html",
  styleUrls: ["./tableau-de-bord-enseigne.component.scss"],
})
export class TableauDeBordEnseigneComponent implements OnInit {
  isBasicStats = false;
  dateModel;
  model;
  datasPdfTampon;
  datasPdfCarte;
  datasPointDeVente;
  pdfTampon;
  pdfCarte;
  pointDeVente;
  enseigne;
  itemStats;
  dataRecompEnAttente;
  optionsRecompEnAttente;
  dataTauxReachat;
  optionsTauxReachat;
  dataNPS;
  optionsNPS;
  optionsTauxRetention;
  dataTauxRetention;
  dataTauxAttrition;
  optionsTauxAttrition;
  dataRecompRembourser;
  optionsRecompRembourser;
  dataRecompEngager;
  optionsRecompEngager;
  optionsCaPdfCarte;
  dataCaPdfCarte;
  optionsCaParPdfCarte;
  optionsInscritsParPdfCarte;
  dataInscritsPdf;
  optionsInscritsPdf;
  optionsInscritsParPdfTampon;
  themeSubscription: any;
  optionsUser;
  currentDateFin = new Date();
  currentDateDebut = new Date();
  currentEnseigneId;
  constructor(
    private theme: NbThemeService,
    private utilitiesService: UtilitiesService,
    private metiersService: MetiersService,
    private route: ActivatedRoute
  ) {}
  ngOnInit() {
    this.currentEnseigneId = this.route.snapshot.params[environment.enseigneId];
    if (this.currentEnseigneId) {
      let data = new User();

      this.currentDateDebut.setFullYear(
        this.currentDateDebut.getFullYear() - 1
      ); // un an en arrière
      this.currentDateDebut.setDate(this.currentDateDebut.getDate() + 1); // ajout d'un jour

      this.dateModel = {
        start: this.currentDateDebut,
        end: this.currentDateFin,
      };

      data.id = this.currentEnseigneId;
      this.metiersService.getBasicStat(environment.enseigne, data).subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (response != null && !response.hasError) {
            this.enseigne = response.items[0];
          }
        },
        (err) => {
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
        }
      );

      this.metiersService.getByCriteria(environment.enseigne, data).subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (response != null && !response.hasError) {
            if (response.items) {
              this.model = response.items[0];
            }
          }
        },
        (err) => {
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
        }
      );

      let dataPdf = new User();
      dataPdf.enseigneId = this.currentEnseigneId;
      this.metiersService
        .getByCriteriaForStats(environment.programmeFideliteCarte, dataPdf)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            if (response != null && !response.hasError) {
              this.datasPdfCarte = response.items;
            }
          },
          (err) => {
            this.utilitiesService.showToast(
              environment.erreurDeConnexion,
              this.utilitiesService.statusInfo
            );
          }
        );
      this.metiersService
        .getByCriteriaForStats(environment.programmeFideliteTampon, dataPdf)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            if (response != null && !response.hasError) {
              this.datasPdfTampon = response.items;
            }
          },
          (err) => {
            this.utilitiesService.showToast(
              environment.erreurDeConnexion,
              this.utilitiesService.statusInfo
            );
          }
        );
      this.metiersService
        .getByCriteriaForStats(environment.pointDeVente, dataPdf)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            if (response != null && !response.hasError) {
              this.datasPointDeVente = response.items;
            }
          },
          (err) => {
            this.utilitiesService.showToast(
              environment.erreurDeConnexion,
              this.utilitiesService.statusInfo
            );
          }
        );

      this.getStats();
    } else {
    }
  }

  getStats(
    dateDebut = this.utilitiesService.dateToString(this.currentDateDebut),
    dateFin = this.utilitiesService.dateToString(this.currentDateFin)
  ) {
    let dataStats = new User();

    dataStats.id = this.currentEnseigneId;
    dataStats.dateDebut = dateDebut;
    dataStats.dateFin = dateFin;
    if (this.pdfCarte) {
      dataStats.programmeFideliteCarteId = this.pdfCarte.id;
    }
    if (this.pdfTampon) {
      dataStats.programmeFideliteTamponId = this.pdfTampon.id;
    }
    if (this.pointDeVente) {
      dataStats.pointDeVenteId = this.pointDeVente.id;
    }

    this.metiersService
      .getStatistiques(environment.enseigne, dataStats)
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (response != null && !response.hasError) {
            if (response.items && response.items.length > 0) {
              this.itemStats = response.items[0];

              // mettre la logique

              this.themeSubscription = this.theme
                .getJsTheme()
                .subscribe((config) => {
                  const colors: any = config.variables;
                  const chartjs: any = config.variables.chartjs;
                  const echarts: any = config.variables.echarts;

                  this.optionsInscritsParPdfTampon = {
                    backgroundColor: echarts.bg,
                    color: [
                      colors.warningLight,
                      colors.infoLight,
                      colors.dangerLight,
                      colors.successLight,
                      colors.primaryLight,
                    ],
                    tooltip: {
                      trigger: "item",
                      formatter: "{a} <br/>{b} : {c} ({d}%)",
                    },
                    legend: {
                      orient: "vertical",
                      left: "left",
                      //data: ["USA", "Germany", "France", "Canada", "Russia"],
                      data: this.itemStats.legendPdfTampon,
                      textStyle: {
                        color: echarts.textColor,
                      },
                    },
                    series: [
                      {
                        name: "Inscrits",
                        type: "pie",
                        radius: "80%",
                        center: ["50%", "50%"],
                        //  data: [
                        //    { value: 335, name: "Germany" },
                        //    { value: 310, name: "France" },
                        //    { value: 234, name: "Canada" },
                        //    { value: 135, name: "Russia" },
                        //    { value: 1548, name: "USA" },
                        //  ],
                        data: this.itemStats.inscritParPdfTampon,
                        itemStyle: {
                          emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: echarts.itemHoverShadowColor,
                          },
                        },
                        label: {
                          normal: {
                            textStyle: {
                              color: echarts.textColor,
                            },
                          },
                        },
                        labelLine: {
                          normal: {
                            lineStyle: {
                              color: echarts.axisLineColor,
                            },
                          },
                        },
                      },
                    ],
                  };
                  this.optionsInscritsParPdfCarte = {
                    backgroundColor: echarts.bg,
                    color: [
                      colors.warningLight,
                      colors.infoLight,
                      colors.dangerLight,
                      colors.successLight,
                      colors.primaryLight,
                    ],
                    tooltip: {
                      trigger: "item",
                      formatter: "{a} <br/>{b} : {c} ({d}%)",
                    },
                    legend: {
                      orient: "vertical",
                      left: "left",
                      //data: ["USA", "Germany", "France", "Canada", "Russia"],
                      data: this.itemStats.legendPdfCarte,

                      textStyle: {
                        color: echarts.textColor,
                      },
                    },
                    series: [
                      {
                        name: "Inscrits",
                        type: "pie",
                        radius: "80%",
                        center: ["50%", "50%"],
                        //  data: [
                        //    { value: 335, name: "Germany" },
                        //    { value: 310, name: "France" },
                        //    { value: 234, name: "Canada" },
                        //    { value: 135, name: "Russia" },
                        //    { value: 1548, name: "USA" },
                        //  ],
                        data: this.itemStats.inscritParPdfCarte,

                        itemStyle: {
                          emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: echarts.itemHoverShadowColor,
                          },
                        },
                        label: {
                          normal: {
                            textStyle: {
                              color: echarts.textColor,
                            },
                          },
                        },
                        labelLine: {
                          normal: {
                            lineStyle: {
                              color: echarts.axisLineColor,
                            },
                          },
                        },
                      },
                    ],
                  };
                  this.optionsCaParPdfCarte = {
                    backgroundColor: echarts.bg,
                    color: [
                      colors.warningLight,
                      colors.infoLight,
                      colors.dangerLight,
                      colors.successLight,
                      colors.primaryLight,
                    ],
                    tooltip: {
                      trigger: "item",
                      formatter: "{a} <br/>{b} : {c} ({d}%)",
                    },
                    legend: {
                      orient: "vertical",
                      left: "left",
                      // data: ["USA", "Germany", "France", "Canada", "Russia"],
                      data: this.itemStats.legendCaPdfCarte,

                      textStyle: {
                        color: echarts.textColor,
                      },
                    },
                    series: [
                      {
                        name: "Chiffre d'affaire",
                        type: "pie",
                        radius: "80%",
                        center: ["50%", "50%"],
                        //  data: [
                        //    { value: 335, name: "Germany" },
                        //    { value: 310, name: "France" },
                        //    { value: 234, name: "Canada" },
                        //    { value: 135, name: "Russia" },
                        //    { value: 1548, name: "USA" },
                        //  ],
                        data: this.itemStats.chiffreDaffaireParPdfCarte,
                        itemStyle: {
                          emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: echarts.itemHoverShadowColor,
                          },
                        },
                        label: {
                          normal: {
                            textStyle: {
                              color: echarts.textColor,
                            },
                          },
                        },
                        labelLine: {
                          normal: {
                            lineStyle: {
                              color: echarts.axisLineColor,
                            },
                          },
                        },
                      },
                    ],
                  };

                  this.dataInscritsPdf = {
                    // labels: [
                    //   "2006",
                    //   "2007",
                    //   "2008",
                    //   "2009",
                    //   "2010",
                    //   "2011",
                    //   "2012",
                    // ],
                    labels: this.itemStats.abscisseGraphe,
                    datasets: [
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.nbreInscritPdfCarte,
                        label: "Programme à points",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.primaryLight,
                          0.8
                        ),
                      },
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.nbreInscritPdfTampon,
                        label: "Programme à tampons",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.infoLight,
                          0.8
                        ),
                      },
                    ],
                  };
                  this.optionsInscritsPdf = {
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                      labels: {
                        fontColor: chartjs.textColor,
                      },
                    },
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: false,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: true,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                    },
                  };

                  this.dataCaPdfCarte = {
                    // labels: [
                    //   "2006",
                    //   "2007",
                    //   "2008",
                    //   "2009",
                    //   "2010",
                    //   "2011",
                    //   "2012",
                    // ],
                    labels: this.itemStats.abscisseGraphe,
                    datasets: [
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.chiffreDaffaireParPdfCarte,
                        label: "Programme à points",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.primaryLight,
                          0.8
                        ),
                      },
                      // {
                      //   data: [28, 48, 40, 19, 86, 27, 90],
                      //   label: "Programme à tampons",
                      //   backgroundColor: NbColorHelper.hexToRgbA(
                      //     colors.infoLight,
                      //     0.8
                      //   ),
                      // },
                    ],
                  };
                  this.optionsCaPdfCarte = {
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                      labels: {
                        fontColor: chartjs.textColor,
                      },
                    },
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: false,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: true,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                    },
                  };

                  this.dataRecompEngager = {
                    // labels: [
                    //   "2006",
                    //   "2007",
                    //   "2008",
                    //   "2009",
                    //   "2010",
                    //   "2011",
                    //   "2012",
                    // ],
                    labels: this.itemStats.abscisseGraphe,
                    datasets: [
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.recompenseEngagePdfCarte,
                        label: "Programme à points",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.primaryLight,
                          0.8
                        ),
                      },
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.recompenseEngagePdfTampon,
                        label: "Programme à tampons",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.infoLight,
                          0.8
                        ),
                      },
                    ],
                  };
                  this.optionsRecompEngager = {
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                      labels: {
                        fontColor: chartjs.textColor,
                      },
                    },
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: false,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: true,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                    },
                  };

                  this.dataRecompRembourser = {
                    // labels: [
                    //   "2006",
                    //   "2007",
                    //   "2008",
                    //   "2009",
                    //   "2010",
                    //   "2011",
                    //   "2012",
                    // ],
                    labels: this.itemStats.abscisseGraphe,
                    datasets: [
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.recompenseRemboursePdfCarte,
                        label: "Programme à points",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.primaryLight,
                          0.8
                        ),
                      },
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.recompenseRemboursePdfTampon,
                        label: "Programme à tampons",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.infoLight,
                          0.8
                        ),
                      },
                    ],
                  };
                  this.optionsRecompRembourser = {
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                      labels: {
                        fontColor: chartjs.textColor,
                      },
                    },
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: false,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: true,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                    },
                  };

                  this.dataTauxAttrition = {
                    // labels: [
                    //   "2006",
                    //   "2007",
                    //   "2008",
                    //   "2009",
                    //   "2010",
                    //   "2011",
                    //   "2012",
                    // ],
                    labels: this.itemStats.abscisseGraphe,
                    datasets: [
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.tauxAttrioPdfCarte,
                        label: "Programme à points",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.primaryLight,
                          0.8
                        ),
                      },
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.tauxAttrioPdfTampon,
                        label: "Programme à tampons",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.infoLight,
                          0.8
                        ),
                      },
                    ],
                  };
                  this.optionsTauxAttrition = {
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                      labels: {
                        fontColor: chartjs.textColor,
                      },
                    },
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: false,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: true,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                    },
                  };

                  this.dataTauxRetention = {
                    // labels: [
                    //   "2006",
                    //   "2007",
                    //   "2008",
                    //   "2009",
                    //   "2010",
                    //   "2011",
                    //   "2012",
                    // ],
                    labels: this.itemStats.abscisseGraphe,
                    datasets: [
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.tauxRetentioPdfCarte,
                        label: "Programme à points",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.primaryLight,
                          0.8
                        ),
                      },
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.tauxRetentioPdfTampon,
                        label: "Programme à tampons",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.infoLight,
                          0.8
                        ),
                      },
                    ],
                  };
                  this.optionsTauxRetention = {
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                      labels: {
                        fontColor: chartjs.textColor,
                      },
                    },
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: false,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: true,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                    },
                  };

                  this.dataNPS = {
                    // labels: [
                    //   "2006",
                    //   "2007",
                    //   "2008",
                    //   "2009",
                    //   "2010",
                    //   "2011",
                    //   "2012",
                    // ],
                    labels: this.itemStats.abscisseGraphe,
                    datasets: [
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.npsPdfCarte,
                        label: "Programme à points",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.primaryLight,
                          0.8
                        ),
                      },
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.npsPdfTampon,
                        label: "Programme à tampons",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.infoLight,
                          0.8
                        ),
                      },
                    ],
                  };
                  this.optionsNPS = {
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                      labels: {
                        fontColor: chartjs.textColor,
                      },
                    },
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: false,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: true,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                    },
                  };

                  this.dataTauxReachat = {
                    // labels: [
                    //   "2006",
                    //   "2007",
                    //   "2008",
                    //   "2009",
                    //   "2010",
                    //   "2011",
                    //   "2012",
                    // ],
                    labels: this.itemStats.abscisseGraphe,
                    datasets: [
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.tauxReachatPdfCarte,
                        label: "Programme à points",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.primaryLight,
                          0.8
                        ),
                      },
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.tauxReachatPdfTampon,
                        label: "Programme à tampons",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.infoLight,
                          0.8
                        ),
                      },
                    ],
                  };
                  this.optionsTauxReachat = {
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                      labels: {
                        fontColor: chartjs.textColor,
                      },
                    },
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: false,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: true,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                    },
                  };

                  this.dataRecompEnAttente = {
                    // labels: [
                    //   "2006",
                    //   "2007",
                    //   "2008",
                    //   "2009",
                    //   "2010",
                    //   "2011",
                    //   "2012",
                    // ],
                    labels: this.itemStats.abscisseGraphe,
                    datasets: [
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.recompenseEnAttentePdfCarte,
                        label: "Programme à points",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.primaryLight,
                          0.8
                        ),
                      },
                      {
                        //data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.itemStats.recompenseEnAttentePdfTampon,
                        label: "Programme à tampons",
                        backgroundColor: NbColorHelper.hexToRgbA(
                          colors.infoLight,
                          0.8
                        ),
                      },
                    ],
                  };
                  this.optionsRecompEnAttente = {
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                      labels: {
                        fontColor: chartjs.textColor,
                      },
                    },
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: false,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: true,
                            color: chartjs.axisLineColor,
                          },
                          ticks: {
                            fontColor: chartjs.textColor,
                          },
                        },
                      ],
                    },
                  };
                });
            }
          }
        },
        (err) => {
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
        }
      );
  }
  changeDebut(date) {
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }

  setIsBasicStats() {
    this.isBasicStats = !this.isBasicStats;
  }

  search() {
    this.getStats(
      this.utilitiesService.dateToString(this.dateModel.start),
      this.utilitiesService.dateToString(this.dateModel.end)
    );
  }
}
