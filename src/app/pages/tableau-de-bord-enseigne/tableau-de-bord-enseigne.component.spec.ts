import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauDeBordEnseigneComponent } from './tableau-de-bord-enseigne.component';

describe('TableauDeBordEnseigneComponent', () => {
  let component: TableauDeBordEnseigneComponent;
  let fixture: ComponentFixture<TableauDeBordEnseigneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableauDeBordEnseigneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableauDeBordEnseigneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
